<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ChartController;
use App\Http\Controllers\API\RolesController;
use App\Http\Controllers\API\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('searchUser',[UserController::class,'searchUser']);
Route::group(['middleware'=>'auth:api'],function(){
    Route::get('profile-details',[AuthController::class,'userDetails']);
    Route::get('roles',[RolesController::class,'index']);
    Route::get('countRole',[ChartController::class,'getCountRole']);
    Route::get('userRegisterCount',[ChartController::class,'getRegisterUserCountByDate']);
    Route::resource('users', UserController::class);

});

Route::post('login',[AuthController::class,'userLogin'])->name('login');
