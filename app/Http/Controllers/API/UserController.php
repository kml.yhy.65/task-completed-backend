<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = User::with('roles')->get();
        return response()->json(['users' => $users]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $user = auth('api')->user();
        if ($user->hasRole('Super Admin')) {
            $validation = Validator::make($request->all(), [
                'name' => 'min:3|required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:3',
                'role' => 'required'
            ]);
            if ($validation->fails()) {
                return response()->json(['error' => $validation->errors()], 422);
            }
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            $user->syncRoles($request->role);
            if ($user) {
                return response()->json($user, 200);
            } else {
                return response()->json(['error' => 'Kayıt Başarısız'], 402);
            }
        } else {
            return response()->json(['message' => 'Erişim Yetkiniz Yok'],402);
        }
    }

    public function show($id)
    {
        $user = User::with('roles')->find($id);
        if ($user) {
            return response()->json($user, 200);
        } else {
            return response()->json(['message' => 'Kullanıcı Bulunamadı'], 402);
        }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
        $user = auth('api')->user();
        if ($user->hasRole('Super Admin')||$user->id==$id) {
            $validation = Validator::make($request->all(), [
                'name' => 'min:3|required',
                'email' => 'required|email|unique:users,email,' . $id,
                'password' => 'min:3|sometimes',
                'role' => 'required'
            ]);
            if ($validation->fails()) {
                return response()->json(['error' => $validation->errors()], 422);
            }
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            $user->update();
            $user->syncRoles($request->role);
            if ($user) {
                return response()->json($user, 200);
            } else {
                return response()->json(['error' => 'Kayıt Başarısız'], 402);
            }
        } else {
            return response()->json(['message' => 'Erişim Yetkiniz Yok'],402);
        }
    }


    public function destroy($id)
    {
        $user = auth('api')->user();
        if ($user->hasRole('Super Admin')) {
            $user=User::find($id);
            $user->delete();
            return response()->json(['message'=>'success'],200);
        }else{
            return response()->json(['message'=>'Erişim Yetkiniz Yok'],402);
        }
    }

    public function searchUser(Request $request)
    {
        $findUser=User::with('roles')->where('name', 'LIKE', '%' . $request->q . '%')
        ->orWhere('email', 'LIKE', '%' . $request->q . '%')->get();
        if($findUser){
            return response()->json(['users' => $findUser],200);
        }else{
            return response()->json(['message'=>'Hiç bir veri bulunamadı'],402);
        }
    }
}
