<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
class AuthController extends Controller
{
    public function userLogin(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required || email',
            'password' => 'required'
        ]);
        if ($validation->fails()) {
            return response()->json(['error' => $validation->errors()], 422);
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $token = $user->createToken('MyApp')->accessToken;
            $role=$user->getRoleNames();
            return response()->json(['token'=>$token,'role'=>$role]);
        }else{
            return response()->json(['error'=>"Not Found User"],403);
        }
    }

    public function userDetails()
    {
        $user = Auth::user();
        return response()->json($user);
    }
}
