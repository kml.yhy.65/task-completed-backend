<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ModelHasRoles;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function getCountRole()
    {
        $dataChart = [];
        $user = ModelHasRoles::with('roles')->get()->groupBy('roles.name');
        foreach ($user as $key => $data) {
            array_push($dataChart, [
                'name' => $key,
                'y' => count($data)
            ]);
        }
        return response()->json($dataChart);
    }
    public function getRegisterUserCountByDate()
    {
        $dataDate = [];
        $registerUserCount = User::select('created_at')
            ->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d.m.Y'); // grouping by years
            });
        foreach ($registerUserCount as $key => $data) {
            array_push($dataDate, [
                'name' => $key,
                'y' => count($data)
            ]);
        }
        return response()->json($dataDate);
    }
}
